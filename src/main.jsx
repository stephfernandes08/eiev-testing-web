import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getDatabase, ref, onValue } from "firebase/database";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDt69BgiRDJXAW53-zpFKaAoky661VU0lU",
  authDomain: "eiev-testing-30355.firebaseapp.com",
  databaseURL: "https://eiev-testing-30355-default-rtdb.firebaseio.com",
  projectId: "eiev-testing-30355",
  storageBucket: "eiev-testing-30355.appspot.com",
  messagingSenderId: "468655946641",
  appId: "1:468655946641:web:40e64e6d468e85dc873830",
  measurementId: "G-DYFW2HSZ7J",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const db = getDatabase();
const raceRef = ref(db, "races/" + "race484");
onValue(raceRef, (snapshot) => {
  const data = snapshot.val();
  console.log("fetched data", data);
});

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
