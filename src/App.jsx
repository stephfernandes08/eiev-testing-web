import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

import { getDatabase, ref, set } from "firebase/database";

function App() {
  const [count, setCount] = useState(0)

  // const SECOND_MS = 5000;

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     console.log('Logs every 10 seconds');
  //     // writeUserData()
  //   }, SECOND_MS);

  //   return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
  // }, [])

  // function writeUserData() {
  //   const db = getDatabase();

  //   set(ref(db, 'races/race1241/riders/0', {
  //     horse_breed: 'sldihg', //'uknw',
  //     horse_gender: "YUNG MALE",//"Gelding"
  //     rider_gender: "Male",//"Female"
  //     rider_name: "sdfsdgdg",//"Masa Adnan Tonbakji"
  //     final_position: "2", //"1"
  //     horse_color: "Greyyyy", //"Bay" },
  //   }));

  // }

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
